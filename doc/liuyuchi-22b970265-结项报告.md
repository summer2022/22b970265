
# liuyuchi-22b970265-结项报告  {ignore=true}

# 目录  {ignore=true}

[toc]
<div STYLE="page-break-after: always;"></div>

# 一、项目信息	
## 1.1 项目名称
迁移基于ROS2和激光导航的算法至OpenEuler21.03平台
## 1.2	项目描述
&ensp;&ensp;为了构建完善的基于OpenEuler的ROS2软件生态，需要将以ROS2为载体的部分激光导航算法迁移至OpenEuler。激光导航算法作为当前自动驾驶/AI领域关键的模块之一，能在OpenEuler的ROS2上运行起来有着重要意义。当前应用广泛的开源激光导航算法有很多，开源的Gmapping, move_base 等均用C++实现，可以作为很好的入门软件进行学习。还可以作为一个入口，扩展更多的相关软件包进入基于OpenEuler的ROS2软件生态。  
&ensp;&ensp;Gmapping是基于滤波SLAM框架的常用开源SLAM算法。其基本原理基于RBpf粒子滤波算法，即将定位和建图过程分离，先进行定位再进行建图。粒子滤波算法是早期的一种建图算法，其基本原理为机器人不断地通过运动、观测的方式，获取周围环境信息，逐步降低自身位置的不确定度，最终得到准确的定位结果。用上一时刻的地图和运动模型预测当前时刻的位姿，然后计算权重，重采样，更新粒子的地图，如此往复。  
&ensp;&ensp;Gmapping可以实时构建室内地图，在构建小场景地图所需的计算量较小且精度较高。相比Cartographer在构建小场景地图时，Gmapping不需要太多的粒子并且没有回环检测因此计算量小于Cartographer而精度并没有差太多。Gmapping有效利用了车轮里程计信息，这也是Gmapping对激光雷达频率要求低的原因：里程计可以提供机器人的位姿先验。  
&ensp;&ensp;随着场景增大所需的粒子增加，因为每个粒子都携带一幅地图，因此在构建大地图时所需内存和计算量都会增加。因此不适合构建大场景地图。并且没有回环检测，因此在回环闭合时可能会造成地图错位，虽然增加粒子数目可以使地图闭合但是以增加计算量和内存为代价。所以不能像Cartographer那样构建大的地图。Gmapping和Cartographer一个是基于滤波框架SLAM另一个是基于优化框架的SLAM，两种算法都涉及到时间复杂度和空间复杂度的权衡。Gmapping牺牲空间复杂度保证时间复杂度，这就造成Gmapping不适合构建大场景地图。

## 1.3	项目导师
韩昊旻/hanhaomin008@126.com
## 1.4	项目开发者
刘宇驰
## 1.5	项目链接
[请点击连接](https://gitee.com/openeuler/open-source-summer/issues/I55G5F?from=project-issue)

# 二、开发详情
## 2.1 方案描述
设备上至少2台机器:  
ubuntu22.04 作为移植前环境，需要有一套ROS2环境作为源头对照；  
OpenEuler21.03/22.03 作为移植后环境，配套至少一台x86主机用来调试对照。  
方案实施的要点：   
- 选定可用的代码源分支
- 源头、目标操作系统构建
- OBS打包构建
- 建图运行测试

详细参考独立的移植文档；

## 2.2 时间规划
### 项目研发第一阶段（07月01日-08月15日）

- 跑通ubuntu最小环境
- 跑通slam_gmapping的移植
- 尝试navigation2其他包的移植，梳理出哪些包是一个月内完成的，最终梳理出最小交付集合

### 项目研发第二阶段（08月16日-09月30日）
- 跑通本地测试用例
- 本地构建rpm包
- 线上构建rpm包

# 三、项目总结	
## 3.1 项目产出


| 计划项目 | 是否完成| 产出结果 |
| --- | --- |--- |
| 跑通ubuntu原型 | Y | ubuntu20.04+地图包+Gmapping，可建图|
|欧拉系统本地构建Gmapping | Y| 物理机安装欧拉系统、虚拟机安装系统；欧拉21.03+Gmapping+ros2,编译通过 ; 欧拉22.03+Gmapping+ros2,编译通过 |
|线上构建含有gammping的包 |Y |22.03 OBS得到RPM包，可安装 | 

下面把有里程碑的过程分小节展示如下。

### 3.1.1 Gmapping 建图过程

主要命令如下。

#### A 包安装
以X86环境，版本号1.0.0为例
- 下载RPM包
登陆到OBS站点"http://117.78.1.88"，输入  
"http://117.78.1.88/package/binaries/home:asgard:branches:OpenEuler:22.03:LTS/slam_gmapping_ros2/standard_x86_64"，
可以得到slam_gmapping_ros2-1.0.0-1.oe2203.x86_64.rpm，效果图如下。

![](./image/img_rpmByObs.png)



- 安装RPM包
``` sh
sudo rpm -ivh slam_gmapping_ros2-1.0.0-1.oe2203.x86_64.rpm  --nodeps --force
```

#### B 更新环境变量

添加以下环境变量到HOME目录下的.bashrc尾部

``` sh
export STAGEPATH=/opt/ros/foxy/share/stage  
export RMW_IMPLEMENTATION=rmw_fastrtps_cpp
```

编辑完.bashrc后，执行以下命令使环境变量生效
``` sh
source ~/.bashrc
```

#### C 启动激光数据源

启动 stage ros模拟器,用键盘控制里面的模型：
``` sh
source /opt/ros/foxy/setup.sh  
ros2 run stage_ros stageros /opt/ros/foxy/share/stage_ros/world/willow-erratic.world  
ros2 run stage_ros teleop_twist_keyboard.py
```
相关运行效果汇总如下图：

![](./image/img_twist.png)

#### D 更新Gmapping的启动配置
打开slam_gmapping.launch.py，更新以下的配置,
手动修改launch文件中的scan话题为base_scan,
use_sim_time为true，base_frame为base_link，参考如下
``` sh
def generate_launch_description():
    param_substitutions = {
        'scan_topic': 'scan',
        'base_frame': 'base_link',
        'use_sim_time': True
    }
```

修改过程如下，相关截图如下：
``` sh
cd /opt/ros/foxy/share/slam_gmapping  
vim /opt/ros/foxy/share/slam_gmapping/launch/slam_gmapping.launch.py  
```
![](./image/img_launch.png)



#### E 启动建图终端


``` sh
source /opt/ros/foxy/setup.bash     
ros2 launch slam_gmapping slam_gmapping.launch.py
```
启动命令如上，启动后终端效果如下：
![](./image/img_launch_run.png)

观察日志，是否时刻更新，如果没有请查看相关topic信息

#### F 启动rviz2观察map
启动命令是
``` sh
rviz2
```
启动后中，要修改fixed frame为base_link，以及添加话题topic为/map  

![](./image/img_map.png)

如果没有显示期望的地图，请注意检查相关的topic(base_scan,tf等)



### 3.1.2 OpenEuler 22.03 OBS 线上记录
主要的工作涵盖如下：
- 主要是按照模板添加源码包  
- 更新spec文件  
- 遇到OBS报错及时按照提示修复输入件  

相关下载包请点击连接:[下载地址](https://117.78.1.88/package/binary/download/home:asgard:branches:OpenEuler:22.03:LTS/slam_gmapping_ros2_update/standard_x86_64/x86_64/slam_gmapping_ros2-1.0.0-1.oe2203.x86_64.rpm)


### 3.1.3 源代码结构图之依赖包

如下是源代码结构，特别请注意图中的pip第三方依赖
![](./image/img_source_tree.png)

### 3.1.4 源代码结构图之ros2包

如下是源代码，特别请注意图中的ros2的包，其中高亮行是slam_gmapping,该截图中使用的命令是
``` sh
cd slam_gmapping_ros2-1.0.0 && tree -C -L 3 workspace
```
![](./image/img_source_tree_ros2.png)


## 3.2 方案进度

整体可控
- 完成Ubuntu 22.04环境上Gmapping构建以及运行；
- 完成OBS上的OpenEuler 22.03的Gmapping打包，
- 完成OpenEuler 22.03上stage ros+Gmapping安装和运行。


## 3.3 遇到的问题及解决方案

### 3.3.1 激光数据源异常
####  slam_gmapping跑不起来分析

问题拆解的思路如下：  
- 确认可用的slam_gmapping的foxy移植分支  
- 可用的激光数据源导入  
- 最小系统的跑通（地图scan_topic和Gmapping的输入scan不匹配的判定）  

### 3.3.2 操作系统环境跳坑记录
  
- 物理机和虚拟机的选择    
&ensp;&ensp;本次算法移植，真机安装欧拉系统和虚拟机安装欧拉系统对比来说，差异不是特别明显  
- 虚拟机对外交互很重要   
&ensp;&ensp;首先虚拟机需要一个可以访问外网的稳定网络，最好开机自启，不用命令等极客方式干预；对virtualbox来说NAT模式下绑定个人主机物理LAN口比较稳定，暂时没摸索出绑定无线网卡的方案，可能当前欧拉系统对于wifi驱动支持欠佳；另外vitualbox的助手包也有必要，方便构建共享文件夹拷贝数据  

- 22.03虚拟机奔溃后的经验总结：镜像保存  
&ensp;&ensp;曾经装过21.03，当时匆忙想装22.03的rpm包，原以为向下兼容，结果发现升级若干系统包后还带来gcc无法回滚，得不偿失，当时也没有保存虚拟机的景象，只能重装系统，如果有一个镜像就可以省不少时间  

### 3.3.3 OBS平台实践去坑
  
- 确认项目、异常日志的判定  
- OBS spec的撰写（权限相关等）  

## 3.4 项目完成质量

&ensp;&ensp;基本达标。  
&ensp;&ensp;如果没有受疫情和实验室工作影响，可以达到更多导航包的移植。  

## 3.5 与导师沟通及反馈情况

- 定期沟通  
&ensp;&ensp;每周一更新阶段任务方向，每周五总结阶段任务
- 及时调整方向、确认核心矛盾  
&ensp;&ensp;前期方向比较发散，后期由于操作系统生态、OBS稳定性等原因，不断调整阶段的目标，确认最小系统可用、稳定。  
- 心态沟通  
&ensp;&ensp;技术的探索不仅仅需要一个未知的好奇心，好需要一颗恒心；遇到困难的时候，梳理方向，聚焦难点；  


# 四、其他参考
非常感谢优秀的导师团队给我们一路的鼓励以及指导。  

[OpenEuler22.03-ROS-SIG-开源之夏指导-RPM包构建与安装](https://www.bilibili.com/video/BV1FG411b7qp/)

[OpenEuler22.03 OBS服务器无法编译临时解决办法](https://www.bilibili.com/video/BV1Ka411V7CM/)

[OpenEuler22.03-ROS-SIG-开源之夏指导-ROS1-RPM包使用和构建](https://www.bilibili.com/video/BV14a411R7tm)

[计划书文档：1_迁移基于ROS2和激光导航的算法到欧拉22.03](http://note.youdao.com/noteshare?id=442d78898dac2b2b1976909240e67008)
