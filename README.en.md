# slam_gmapping_ros2

#### Description
In order to build a complete OpenEuler-based ROS2 software ecosystem, some navigation algorithms based on ROS2 need to be migrated to OpenEuler. As one of the key modules in the current autonomous driving/AI field, the navigation algorithm is of great significance to run on OpenEuler's ROS2. There are many open source yolo algorithms that are widely used at present. Open source yolo/cnn are all implemented in C++, which can be used as a good entry software for learning. It can also be used as an entry to expand more related software packages into the OpenEuler-based ROS2 software ecosystem.

#### Software Architecture
The original architecture graph can be referred from https://github.com/Project-MANAS/slam_gmapping

The core software structure is as follows, we can get full view of the workspace directory, also we can see the distribution locations of ros2 and slam_gmapping.
``` sh
.
├── 3rdparty
├── build_tools
│   └── colcon
└── workspace
    └── src
        ├── ament
        │   ├── ament_cmake
        │   ├── ament_index
        │   ├── ament_lint
        │   ├── ament_package
        │   ├── google_benchmark_vendor
        │   ├── googletest
        │   └── uncrustify_vendor
        ├── eclipse-cyclonedds
        │   └── cyclonedds
        ├── eProsima
        │   ├── Fast-CDR
        │   ├── Fast-DDS
        │   └── foonathan_memory_vendor
        ├── osrf
        │   ├── osrf_pycommon
        │   └── osrf_testing_tools_cpp
        ├── ros
        │   ├── class_loader
        │   ├── pluginlib
        │   ├── resource_retriever
        │   ├── ros_environment
        │   ├── ros_tutorials
        │   ├── Stage
        │   ├── stage_ros2
        │   ├── urdfdom
        │   └── urdfdom_headers
        ├── ros2
        │   ├── ament_cmake_ros
        │   ├── common_interfaces
        │   ├── console_bridge_vendor
        │   ├── demos
        │   ├── eigen3_cmake_module
        │   ├── example_interfaces
        │   ├── examples
        │   ├── geometry2
        │   ├── launch
        │   ├── launch_ros
        │   ├── libyaml_vendor
        │   ├── message_filters
        │   ├── mimick_vendor
        │   ├── orocos_kinematics_dynamics
        │   ├── performance_test_fixture
        │   ├── python_cmake_module
        │   ├── rcl
        │   ├── rclcpp
        │   ├── rcl_interfaces
        │   ├── rcl_logging
        │   ├── rclpy
        │   ├── rcpputils
        │   ├── rcutils
        │   ├── realtime_support
        │   ├── rmw
        │   ├── rmw_connext
        │   ├── rmw_cyclonedds
        │   ├── rmw_dds_common
        │   ├── rmw_fastrtps
        │   ├── rmw_implementation
        │   ├── ros1_bridge
        │   ├── ros2cli
        │   ├── ros2cli_common_extensions
        │   ├── rosidl
        │   ├── rosidl_dds
        │   ├── rosidl_defaults
        │   ├── rosidl_python
        │   ├── rosidl_runtime_py
        │   ├── rosidl_typesupport
        │   ├── rosidl_typesupport_connext
        │   ├── rosidl_typesupport_fastrtps
        │   ├── ros_testing
        │   ├── rpyutils
        │   ├── slam_gmapping
        │   ├── spdlog_vendor
        │   ├── sros2
        │   ├── system_tests
        │   ├── test_interface_files
        │   ├── tinyxml2_vendor
        │   ├── tinyxml_vendor
        │   ├── tlsf
        │   ├── unique_identifier_msgs
        │   ├── urdf
        │   └── yaml_cpp_vendor
        ├── ros-perception
        │   └── laser_geometry
        ├── ros-planning
        │   └── navigation_msgs
        ├── ros-tooling
        │   └── libstatistics_collector
        ├── ros-tracing
        │   └── ros2_tracing
        ├── ros-visualization
        │   ├── interactive_markers
        │   ├── python_qt_binding
        │   ├── rqt_action
        │   ├── rqt_console
        │   ├── rqt_graph
        │   ├── rqt_msg
        │   ├── rqt_plot
        │   ├── rqt_publisher
        │   ├── rqt_top
        │   ├── rqt_topic
        │   └── tango_icons_vendor
        └── temp_3rdparty
            └── eigen-3.3.7
```
#### Installation
Take the x86-build package with version 1.0.0 for example 
- download package
First login in "http://117.78.1.88"，
input "https://117.78.1.88/package/binaries/home:asgard:branches:OpenEuler:22.03:LTS/slam_gmapping_ros2/standard_x86_64"
to get the pacakge needed, the relative image is shown below.
![](/doc/image/img_rpmByObs.png)

- install package
``` sh
sudo rpm -ivh slam_gmapping_ros2-1.0.0-1.oe2203.x86_64.rpm  --nodeps --force
```

#### Instructions

After installation, launch the rviz,
if the result is the same with the below ,the installation is done.


##### 1 Update environment variables

Add the following environment variables to the end of .bashrc in the HOME directory

```` sh
export STAGEPATH=/opt/ros/foxy/share/stage  
export RMW_IMPLEMENTATION=rmw_fastrtps_cpp
````

After editing .bashrc, execute the following command to make the environment variable take effect
```` sh
source ~/.bashrc
````

##### 2 Start the laser data source

Start the stage ros simulator and use the keyboard to control the model inside:
```` sh
source /opt/ros/foxy/setup.sh  
ros2 run stage_ros stageros /opt/ros/foxy/share/stage_ros/world/willow-erratic.world  
ros2 run stage_ros teleop_twist_keyboard.py  
````
The relevant operating results are summarized as follows:

![](/doc/image/img_twist.png)

##### 3 Update the startup configuration of gmapping
Open slam_gmapping.launch.py ​​and update the following configuration,
Manually modify the scan theme in the launch file to be base_scan,
Manually modify the use_sim_time in the launch file to true, base_frame to be base_link ,refer to the following
```` sh
def generate_launch_description():
    param_substitutions = {  
        'scan_topic': 'scan',  
        'base_frame': 'base_link',  
        'use_sim_time': True  
    }  
````

The modification process is as follows, and the relevant screenshots are as follows:
```` sh
cd /opt/ros/foxy/share/slam_gmapping  
vim /opt/ros/foxy/share/slam_gmapping/launch/slam_gmapping.launch.py  
````

![](/doc/image/img_launch.png)

##### 4 Start the mapping terminal

```` sh
source /opt/ros/foxy/setup.bash     
ros2 launch slam_gmapping slam_gmapping.launch.py  
````
The startup command is as above, and the terminal effect after startup is as follows:
![](/doc/image/img_launch_run.png)

Observe the log, to check whether it is updated all the time, if not, please check the relevant topic information

##### 5 Start rviz2 to observe the map
The startup command is 
``` sh
rviz2
```
After startup, modify the fixed frame as base_link and add the topic as /map 

![](/doc/image/img_map.png)

If the expected map is not displayed, please check the relevant topic (base_scan, tf, etc.)

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
