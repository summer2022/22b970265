# slam_Gmapping_ros2
#### 介绍
为了构建完善的基于OpenEuler的ROS2软件生态，需要将以ROS2为载体的部分激光导航算法迁移至OpenEuler。激光导航算法作为当前自动驾驶/AI领域关键的模块之一，能在OpenEuler的ROS2上运行起来有着重要意义。当前应用广泛的开源激光导航算法有很多，开源的Gmapping,move_base等均用C++实现，可以作为很好的入门软件进行学习。还可以作为一个入口，扩展更多的相关软件包进入基于OpenEuler的ROS2软件生态。

#### 软件架构
软件架构原理参考：https://github.com/Project-MANAS/slam_gmapping

其中核心软件结构如下，对于workspace目录完整展示，可以看到ros2和slam_gmapping的分布位置。
``` sh
.
├── 3rdparty
├── build_tools
│   └── colcon
└── workspace
    └── src
        ├── ament
        │   ├── ament_cmake
        │   ├── ament_index
        │   ├── ament_lint
        │   ├── ament_package
        │   ├── google_benchmark_vendor
        │   ├── googletest
        │   └── uncrustify_vendor
        ├── eclipse-cyclonedds
        │   └── cyclonedds
        ├── eProsima
        │   ├── Fast-CDR
        │   ├── Fast-DDS
        │   └── foonathan_memory_vendor
        ├── osrf
        │   ├── osrf_pycommon
        │   └── osrf_testing_tools_cpp
        ├── ros
        │   ├── class_loader
        │   ├── pluginlib
        │   ├── resource_retriever
        │   ├── ros_environment
        │   ├── ros_tutorials
        │   ├── Stage
        │   ├── stage_ros2
        │   ├── urdfdom
        │   └── urdfdom_headers
        ├── ros2
        │   ├── ament_cmake_ros
        │   ├── common_interfaces
        │   ├── console_bridge_vendor
        │   ├── demos
        │   ├── eigen3_cmake_module
        │   ├── example_interfaces
        │   ├── examples
        │   ├── geometry2
        │   ├── launch
        │   ├── launch_ros
        │   ├── libyaml_vendor
        │   ├── message_filters
        │   ├── mimick_vendor
        │   ├── orocos_kinematics_dynamics
        │   ├── performance_test_fixture
        │   ├── python_cmake_module
        │   ├── rcl
        │   ├── rclcpp
        │   ├── rcl_interfaces
        │   ├── rcl_logging
        │   ├── rclpy
        │   ├── rcpputils
        │   ├── rcutils
        │   ├── realtime_support
        │   ├── rmw
        │   ├── rmw_connext
        │   ├── rmw_cyclonedds
        │   ├── rmw_dds_common
        │   ├── rmw_fastrtps
        │   ├── rmw_implementation
        │   ├── ros1_bridge
        │   ├── ros2cli
        │   ├── ros2cli_common_extensions
        │   ├── rosidl
        │   ├── rosidl_dds
        │   ├── rosidl_defaults
        │   ├── rosidl_python
        │   ├── rosidl_runtime_py
        │   ├── rosidl_typesupport
        │   ├── rosidl_typesupport_connext
        │   ├── rosidl_typesupport_fastrtps
        │   ├── ros_testing
        │   ├── rpyutils
        │   ├── slam_gmapping
        │   ├── spdlog_vendor
        │   ├── sros2
        │   ├── system_tests
        │   ├── test_interface_files
        │   ├── tinyxml2_vendor
        │   ├── tinyxml_vendor
        │   ├── tlsf
        │   ├── unique_identifier_msgs
        │   ├── urdf
        │   └── yaml_cpp_vendor
        ├── ros-perception
        │   └── laser_geometry
        ├── ros-planning
        │   └── navigation_msgs
        ├── ros-tooling
        │   └── libstatistics_collector
        ├── ros-tracing
        │   └── ros2_tracing
        ├── ros-visualization
        │   ├── interactive_markers
        │   ├── python_qt_binding
        │   ├── rqt_action
        │   ├── rqt_console
        │   ├── rqt_graph
        │   ├── rqt_msg
        │   ├── rqt_plot
        │   ├── rqt_publisher
        │   ├── rqt_top
        │   ├── rqt_topic
        │   └── tango_icons_vendor
        └── temp_3rdparty
            └── eigen-3.3.7
```

#### 安装教程
以X86环境，版本号1.0.0为例
- 下载RPM包
登陆到OBS站点"http://117.78.1.88"， 输入  
"https://117.78.1.88/package/binaries/home:asgard:branches:OpenEuler:22.03:LTS/slam_gmapping_ros2/standard_x86_64" ，
可以得到当前版本的PRM包 ，效果图如下。

![](/doc/image/img_rpmByObs.png)


- 安装RPM包
``` sh
sudo rpm -ivh slam_gmapping_ros2-1.0.0-1.oe2203.x86_64.rpm  --nodeps --force
```
#### 使用说明

安装完成以后，rviz有如下输出,则表示安装成功


##### A 更新环境变量

添加以下环境变量到HOME目录下的.bashrc尾部

``` sh
export STAGEPATH=/opt/ros/foxy/share/stage  
export RMW_IMPLEMENTATION=rmw_fastrtps_cpp
```

编辑完.bashrc后，执行以下命令使环境变量生效
``` sh
source ~/.bashrc
```

##### B 启动激光数据源

启动 stage ros模拟器,用键盘控制里面的模型：
``` sh
source /opt/ros/foxy/setup.sh  
ros2 run stage_ros stageros /opt/ros/foxy/share/stage_ros/world/willow-erratic.world  
ros2 run stage_ros teleop_twist_keyboard.py
```
相关运行效果汇总如下图：

![](/doc/image/img_twist.png)

##### C 更新Gmapping的启动配置
打开slam_gmapping.launch.py，更新以下的配置,
手动修改launch文件中的scan话题为base_scan,
use_sim_time为true，base_frame为base_link，参考如下
``` sh
def generate_launch_description():
    param_substitutions = {
        'scan_topic': 'scan',
        'base_frame': 'base_link',
        'use_sim_time': True
    }
```

修改过程如下，相关截图如下：
``` sh
cd /opt/ros/foxy/share/slam_gmapping  
vim /opt/ros/foxy/share/slam_gmapping/launch/slam_gmapping.launch.py
```

![](/doc/image/img_launch.png)

##### D 启动建图终端


``` sh
source /opt/ros/foxy/setup.bash     
ros2 launch slam_gmapping slam_gmapping.launch.py
```
启动命令如上，启动后终端效果如下：
![](/doc/image/img_launch_run.png)

观察日志，是否时刻更新，如果没有请查看相关topic信息

##### E 启动rviz2观察map
启动命令是
``` sh
rviz2
```
启动后中，要修改fixed frame为base_link，以及添加话题topic为/map

![](/doc/image/img_map.png)

如果没有显示期望的地图，请注意检查相关的topic(base_scan,tf等)



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
