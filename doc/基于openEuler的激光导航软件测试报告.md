# 基于OpenEuler的激光导航软件测试报告  {ignore=true}
# 目录  {ignore=true}

[TOC]
<div STYLE="page-break-after: always;"></div>

# 一、背景介绍
&ensp;&ensp;为了构建完善的基于OpenEuler的ROS2软件生态，需要将以ROS2为载体的部分激光导航算法迁移至OpenEuler。激光导航算法作为当前自动驾驶/AI领域关键的模块之一，能在OpenEuler的ROS2上运行起来有着重要意义。当前应用广泛的开源激光导航算法有很多，开源的Gmapping, move_base 等均用C++实现，可以作为很好的入门软件进行学习。还可以作为一个入口，扩展更多的相关软件包进入基于OpenEuler的ROS2软件生态。  
&ensp;&ensp;Gmapping是基于滤波SLAM框架的常用开源SLAM算法。其基本原理基于RBpf粒子滤波算法，即将定位和建图过程分离，先进行定位再进行建图。粒子滤波算法是早期的一种建图算法，其基本原理为机器人不断地通过运动、观测的方式，获取周围环境信息，逐步降低自身位置的不确定度，最终得到准确的定位结果。用上一时刻的地图和运动模型预测当前时刻的位姿，然后计算权重，重采样，更新粒子的地图，如此往复。  
&ensp;&ensp;Gmapping可以实时构建室内地图，在构建小场景地图所需的计算量较小且精度较高。相比Cartographer在构建小场景地图时，Gmapping不需要太多的粒子并且没有回环检测因此计算量小于Cartographer而精度并没有差太多。Gmapping有效利用了车轮里程计信息，这也是Gmapping对激光雷达频率要求低的原因：里程计可以提供机器人的位姿先验。  
&ensp;&ensp;随着场景增大所需的粒子增加，因为每个粒子都携带一幅地图，因此在构建大地图时所需内存和计算量都会增加。因此不适合构建大场景地图。并且没有回环检测，因此在回环闭合时可能会造成地图错位，虽然增加粒子数目可以使地图闭合但是以增加计算量和内存为代价。所以不能像Cartographer那样构建大的地图。Gmapping和Cartographer一个是基于滤波框架SLAM另一个是基于优化框架的SLAM，两种算法都涉及到时间复杂度和空间复杂度的权衡。Gmapping牺牲空间复杂度保证时间复杂度，这就造成Gmapping不适合构建大场景地图。  

# 二、测试环境

## 硬件要求

建议硬件配置如下所示。

| 项目 | 说明 |
| --- | --- |
| CPU | i5算力及以上X86 CPU |
| 磁盘 | >40G,对磁盘分区无要求 |
## 操作系统要求

操作系统要求如下所示。

| 项目 | 版本 |
| --- | --- |
| OpenEuler | 22.03 sp1 x86 |
| Kernel | 5.10 |

## 环境配置
添加以下环境变量到HOME目录下的.bashrc尾部
``` sh
export STAGEPATH=/opt/ros/foxy/share/stage  
export RMW_IMPLEMENTATION=rmw_fastrtps_cpp
```
## launch文件参数更新

打开slam_gmapping.launch.py，更新以下的配置,
手动修改launch文件中的scan话题为base_scan,
use_sim_time为true，base_frame为base_link，参考如下
``` sh
def generate_launch_description():  
    param_substitutions = {  
        'scan_topic': 'scan',  
        'base_frame': 'base_link',  
        'use_sim_time': True  
    }  
```
修改过程如下，相关截图如下：
``` sh
cd /opt/ros/foxy/share/slam_gmapping  
vim /opt/ros/foxy/share/slam_gmapping/launch/slam_gmapping.launch.py
```
![](./image/img_launch.png)

# 三、服务测试
核心命令是：
``` sh
ros2 launch slam_gmapping slam_gmapping.launch.py
```
## 3.1 产生激光扫描数据

### ros2的包处理方法
``` sh
ros2 bag play data_example.bag
```
### stage ros产生数据的方法
``` sh
ros2 run stage_ros stageros /opt/ros/foxy/share/stage_ros/world/willow-erratic.world  
ros2 run stage_ros teleop_twist_keyboard.py
```
## 3.2 建图测试
这个章节其实涵盖了安装与基础功能测试、功能性测试，详细流程如下。

#### A 包安装
以X86环境，版本号1.0.0为例
- 下载RPM包
登陆到OBS站点"http://117.78.1.88"，输入  
"http://117.78.1.88/package/binaries/home:asgard:branches:OpenEuler:22.03:LTS/slam_gmapping_ros2/standard_x86_64"，
可以得到期望的RPM包，效果图如下。

![](./image/img_rpmByObs.png)

- 安装RPM包
``` sh
sudo rpm -ivh slam_gmapping_ros2-1.0.0-1.oe2203.x86_64.rpm  --nodeps --force
```
#### B 更新环境变量

添加以下环境变量到HOME目录下的.bashrc尾部

``` sh
export STAGEPATH=/opt/ros/foxy/share/stage  
export RMW_IMPLEMENTATION=rmw_fastrtps_cpp
```
编辑完.bashrc后，执行以下命令使环境变量生效
``` sh
source ~/.bashrc
```
#### C 启动激光数据源

启动 stage ros模拟器,用键盘控制里面的模型：
``` sh
source /opt/ros/foxy/setup.sh  
ros2 run stage_ros stageros /opt/ros/foxy/share/stage_ros/world/willow-erratic.world  
ros2 run stage_ros teleop_twist_keyboard.py  
```
相关运行效果汇总如下图：
![](./image/img_twist.png)

#### D 更新Gmapping的启动配置
打开slam_gmapping.launch.py，更新以下的配置,
手动修改launch文件中的scan话题为base_scan,
use_sim_time为true，base_frame为base_link，参考如下
``` sh
def generate_launch_description():  
    param_substitutions = {  
        'scan_topic': 'scan',  
        'base_frame': 'base_link',  
        'use_sim_time': True  
    }  
```
修改过程如下，相关截图如下：
``` sh
cd /opt/ros/foxy/share/slam_gmapping  
vim /opt/ros/foxy/share/slam_gmapping/launch/slam_gmapping.launch.py
```
![](./image/img_launch.png)

#### E 启动建图终端
``` sh
source /opt/ros/foxy/setup.bash     
ros2 launch slam_gmapping slam_gmapping.launch.py
```
启动命令如上，启动后终端效果如下：
![](./image/img_launch_run.png)

观察日志，是否时刻更新，如果没有请查看相关topic信息

#### F 启动rviz2观察map
启动命令是
``` sh
rviz2  
```
启动后，要修改fixed frame为base_link，以及添加话题topic为/map

![](./image/img_map.png)

如果没有显示期望的地图，请注意检查相关的topic(base_scan,tf等)

## 3.3 持续构建测试

持续测试方面涵盖如下方面

![](./image/img_ok4ci.png)

特别注意的点有：

- check_binary_file如果遇到异常，请注意清理源码包.o等二进制文件

- check_package_license如果遇到异常，请务必签署CLA协议整个提交过程的git配置信息和CLA需要保持一致

- check_build如果遇到异常，请先自行解读CI的报错日志，对于rviz arm下异常场景，当前做临时删除，如果后续rviz可以使用了，可以后续增加进来。

- check_spec_file如果遇到异常，请特别需要门禁规则，里面版本号、release号以及changelog是一一关联的、甚至里面的日期都要是真实存在的、spec里面的setup tool 、cython、numpy在22.03系统上需要搭配使用，某个版本过高都会引发连环报错


## 3.4 总结
使用本地环境，完成了安装与基础功能测试、功能性测试。  
在OBS上完成RPM包的原始构建。  
在CI上完成了规范性测试。  
以上测试都达到了预期的效果。  
